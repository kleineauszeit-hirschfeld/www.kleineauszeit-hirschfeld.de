#!/bin/bash
. setenv.sh
cp ${CI_PROJECT_NAME} /etc/nginx/sites-available
certbot certonly --nginx --domain ${CI_PROJECT_NAME}
ln -s /etc/nginx/sites-available/${CI_PROJECT_NAME} /etc/nginx/sites-enabled/
nginx -s reload
