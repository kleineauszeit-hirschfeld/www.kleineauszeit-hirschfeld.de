function getQueryVariable(variable){
       var query = window.location.search.substring(1);
       var vars = query.split("&");
       for (var i=0;i<vars.length;i++) {
               var pair = vars[i].split("=");
               if(pair[0] == variable){return pair[1];}
       }
}

function regex( s, p){
  return s.match(eval('/'+p+'/g')) != undefined;
}

function changeUrl(page, url) {
    if (typeof (history.pushState) != "undefined") {
        var obj = { Page: page, Url: url };
        history.pushState(obj, obj.Page, obj.Url);
    } else {
        alert("Browser does not support HTML5.");
    }
}

function json_load( url, type ){
  var json = null;
  $.ajax({
      'type':"GET",
      'async': false,
      'global': false,
      'cache': false,
      'url': url,
      'dataType': type,
      'success': function (data) {
          json = data;
      }
  });
  return json;
}

const sortObject = obj => Object.keys(obj).sort().reduce((res, key) => (res[key] = obj[key], res), {})

/*
var token = function() {
    return Math.random().toString(36).substr(2); // remove `0.`
};
*/

// This script is released to the public domain and may be used, modified and
// distributed without restrictions. Attribution not necessary but appreciated.
// Source: https://weeknumber.net/how-to/javascript

// Returns the ISO week of the date.
Date.prototype.getWeek = function() {
  var date = new Date(this.getTime());
  date.setHours(0, 0, 0, 0);
  // Thursday in current week decides the year.
  date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7);
  // January 4 is always in week 1.
  var week1 = new Date(date.getFullYear(), 0, 4);
  // Adjust to Thursday in week 1 and count number of weeks from date to week1.
  return 1 + Math.round(((date.getTime() - week1.getTime()) / 86400000
                        - 3 + (week1.getDay() + 6) % 7) / 7);
}

// Returns the four-digit year corresponding to the ISO week of the date.
Date.prototype.getWeekYear = function() {
  var date = new Date(this.getTime());
  date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7);
  return date.getFullYear();
}

class ClientRequest {
  constructor(f) {
    this.f = f;
  }
  post(func){
    $.post('client.php',this,func);
  }
}


function cclientRequest(f){

  $.post('client.php',
    {
        f: 'getPaymentById',
        payment_id: payment_id,
    },
    function(RESULT, STATUS){
      if( STATUS != "success") alert("Data: " + RESULT + "\nStatus: " + STATUS);
      console.log( 'getByPaymentId()', RESULT );
      RESULT=JSON.parse(RESULT);
      console.log(RESULT);

      if( RESULT.length == 1 ){

        var gateway_key=RESULT[0].gateway_key.replace(/ /g,'').toUpperCase();
        var md4gateway_key=hex_md4(gateway_key);

        if( GATEWAY_LIST[md4gateway_key] != undefined ) getByGateway(GATEWAY_LIST[md4gateway_key],gateway_key);

        $("#payment_id").text(RESULT[0].payment_id);
        $("#tokeninfo p").text("You need the token below for checking the state of transaction or getting support from Stargate team!");
        $("#gateway_amount").val( RESULT[0].gateway_amount.toFixed(2) ).attr("readonly",true);
        $('#gateway_currency span').html(RESULT[0].gateway_currency );
        $("#gateway_concept").val(RESULT[0].gateway_concept).attr("readonly",true);;
        //$("#customer_botc_pay_in_fair_address").val(RESULT[0].customer_botc_pay_in_fair_address);
        //$("#customer_botc_pay_in_fair_amount").val( RESULT[0].customer_botc_pay_in_fair_amount.toFixed(8)  );
        //$(".container-fluid *").toggleClass("d-none",false);
        $(".line2").toggleClass("goBackground",true);
        $(".line3").toggleClass("goBackground",true);
        $(".line4").toggleClass("d-none",false);
        $(".line5").toggleClass("d-none",false);

        $("#gateway_calculate").toggleClass("d-none",true);
        $("#gateway_open").toggleClass("btn-primary",false).toggleClass("btn-success",true).text("Stargate is still open");

        $('#status_message').html('..loading payment..');

        makeQRCode("faircoin://" + RESULT[0].customer_botc_pay_in_fair_address + "?amount=" + RESULT[0].customer_botc_pay_in_fair_amount);

        checkPayment();

        changeUrl("FairCoin Stargate",STARGATE_URL + "index.php?payment_id=" + payment_id);

        return true;
      }
   });
}

function getN(n){
  if( n.match(/^N[0-9][0-9][0-9][0-9][0-9]$/g) ){
    return n.slice(1)*1;
  } else {
    return 0;
  }
}
function getN0(n){
  return 'N'+n.toString().padStart(5,'0');
}

function getM(m){
  if( m.match(/^M[0-9][0-9][0-9][0-9][0-9][0-9]$/g) ){
    return m.slice(1)*1;
  } else {
    return 0;
  }
}
function getM0(m){
  return 'M'+n.toString().padStart(6,'0');
}

function get_(id){
  if( id.match(/^[0-9][0-9][0-9][0-9]$/g) ){
    return id.slice(1)*1;
  } else {
    return 0;
  }
}
function get_0(id){
  return id.toString().padStart(4,'0');
}

function copyToClipboard(elem,msg=true) {
  /* Get the text field */
  var copyText = elem;

  /* Select the text field */
  copyText.select();
  copyText.setSelectionRange(0, 99999); /* For mobile devices */

  /* Copy the text inside the text field */
  document.execCommand("copy");
  if( msg ) alert('copied');
}
