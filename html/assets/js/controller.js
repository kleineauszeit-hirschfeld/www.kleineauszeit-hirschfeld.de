function addController(obj){

    var json = null;

    $.ajax({
        'type':"POST",
        'async': true,
        'global': false,
        'cache': false,
        'url': 'set_control.php',
        'dataType': 'text',
        'data' : { 'id' : parseInt($(obj).attr('i')) , 'zeitstempel' : $(obj).attr('zeitstempel'), 'raum' : $(obj).attr('raum'), 'temp' : $(obj).attr('temp'), 'on' : obj.checked },
        'success': function (data) {
            console.log( data );
            json = data;
        }
    });

    return json;

}

$(document).ready(
    function(){
        $('.on').prop('checked',true);
        $('.off').prop('checked',false);
    }
);