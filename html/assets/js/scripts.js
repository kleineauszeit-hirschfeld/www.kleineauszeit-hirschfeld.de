var flag=0;

jQuery(function ($) {

    'use strict';

    // -------------------------------------------------------------
    // Preloader
    // -------------------------------------------------------------
    (function () {
        $('#status').fadeOut();
        $('#preloader').delay(200).fadeOut('slow');
    }());

    //setBackground();

    // ------------------------------------------------------------------
    // sticky menu
    // ------------------------------------------------------------------
	$(window).scroll(function() {

      //navbarScroll();

	});

  //navbarScroll();

  $('.showinfo').on('click', function(obj){
    var h=$($($(obj)[0].target).parent().children()[2]).css('max-height');
    $($($(obj)[0].target).parent().children()[2]).css('max-height', ( (h == '0px') ? '200px' : '0px' ) );
  });

  /*
  $('.meldung_beschreibung_text').on('click', function(obj){
    $('body').css('overflow','hidden',false);
    flag=true;
    setTimeout(function(){ flag=false },100 );
  });
  */

/*
  $('.meldung_exit').on('click', function(obj){
    $('body').css('overflow','hidden',true);
    $('body').css('overflow','initial');
    $($($(obj)[0].target).parent().children()[2]).toggleClass('visible', false);
    $($($($(obj)[0].target).parent().children()[2]).children()[1]).toggleClass('visible',false );
  });
*/

    // -------------------------------------------------------------
    // mobile menu
    // -------------------------------------------------------------
    (function () {
        $('button.navbar-toggle').ucOffCanvasMenu({
        documentWrapper: '#main-wrapper',
        contentWrapper : '.content-wrapper',
        position       : 'uc-offcanvas-left',    // class name
        // opener         : 'st-menu-open',            // class name
        effect         : 'slide-along',             // class name
        closeButton    : '#uc-mobile-menu-close-btn',
        menuWrapper    : '.uc-mobile-menu',                 // class name below-pusher
        documentPusher : '.uc-mobile-menu-pusher'
        });
    }());




    // -------------------------------------------------------------
    // tooltip
    // -------------------------------------------------------------

    (function () {

        $('[data-toggle="tooltip"]').tooltip()

    }());




    // ------------------------------------------------------------------
    // jQuery for back to Top
    // ------------------------------------------------------------------
    (function(){

          $('body').append('<div id="toTop"><i class="fa fa-angle-up"></i></div>');

            $(window).scroll(function () {
                if ($(this).scrollTop() != 0) {
                    $('#toTop').fadeIn();
                } else {
                    $('#toTop').fadeOut();
                }
            });

        $('#toTop').on('click',function(){
            $("html, body").animate({ scrollTop: 0 }, 600);
            return false;
        });

    }());


    // -------------------------------------------------------------
    // testimonialSlider
    // -------------------------------------------------------------
    (function () {

        $('.testimonialSlider').flexslider({
            animation: "slide",
            controlNav: "thumbnails",
            directionNav: false
        })

        // Navigation
        $('.prev').on('click', function(){
            $('.testimonialSlider').flexslider('prev')
            return false;
        })

        $('.next').on('click', function(){
            $('.testimonialSlider').flexslider('next')
            return false;
        })
    }());


    $('a').click(function(){
      if( ! $(this).hasClass('carousel-control') ){
        $('html, body').animate({
            scrollTop: $( $(this).attr('href') ).offset().top
        }, 500);
        return false;
      }
    });

}); // JQuery end

function navbarScroll(){

  if ($(".navbar").offset().top > 50) {
      $(".navbar-fixed-top").addClass("sticky-nav");
  } else {
      $(".navbar-fixed-top").removeClass("sticky-nav");
  }

  var opacity=( ($(".navbar").offset().top - 200)/600)*1;
  opacity=Math.min(opacity,0.8);
  opacity=Math.max(opacity,0);

  //if ($(".navbar").offset().top > 300) {
      $(".navbar-fixed-top").css('background-color','rgba(66,66,100,'+opacity+')');
  //}

  $('.navbar-nav .active').toggleClass('active',false);

  if(window.scrollY + 50 > posY( $('#kontakt')[0])){
    $('#li_kontakt').toggleClass('active', true );
    return;
  }

  if(window.scrollY + 50 > posY( $('#salzgrotte')[0])) {
    $('#li_news_termine').toggleClass('active', true );
    return;
  }

  if(window.scrollY + 50 > posY( $('#wellness')[0])) {
    $('#li_news_termine').toggleClass('active', true );
    return;
  }

  if(window.scrollY + 50 > posY( $('#start')[0]) ){
    $('#li_start').toggleClass('active', true );
    return;
  }

}

function navbarToggle(){
  $('.navbar-nav').toggleClass('show'); $('#navbar-collapse-1').toggleClass('collapse'); 
  $('.uc-mobile-menu-pusher').toggleClass('uc-offcanvas-pusher',$('.navbar-nav').hasClass('show'));
}

function posY(obj)
{
  var curtop = 0;

  if( obj.offsetParent )
  {
    while(1)
    {
      curtop += obj.offsetTop;

      if( !obj.offsetParent )
      {
        break;
      }

      obj = obj.offsetParent;
    }
  } else if( obj.y )
    {
     curtop += obj.y;
    }

  return curtop;
}

var BIMG=[];
BIMG[0]='hintergrund1i.jpg';
BIMG[1]='hintergrund1i.jpg';
BIMG[2]='hintergrund1i.jpg';
BIMG[3]='hintergrund1i.jpg';
BIMG[4]='hintergrund1i.jpg';
BIMG[5]='hintergrund1i.jpg';
BIMG[6]='hintergrund1c.jpg';
BIMG[7]='hintergrund1d.jpg';
BIMG[8]='hintergrund1d.jpg';
BIMG[9]='hintergrund1d.jpg';
BIMG[10]='hintergrund1b.jpg';
BIMG[11]='hintergrund1b.jpg';
BIMG[12]='hintergrund1b.jpg';
BIMG[13]='hintergrund1b.jpg';
BIMG[14]='hintergrund1b.jpg';
BIMG[15]='hintergrund1g.jpg';
BIMG[16]='hintergrund1g.jpg';
BIMG[17]='hintergrund1g.jpg';
BIMG[18]='hintergrund1g.jpg';
BIMG[19]='hintergrund1h.jpg';
BIMG[20]='hintergrund1d.jpg';
BIMG[21]='hintergrund1b.jpg';
BIMG[22]='hintergrund1i.jpg';
BIMG[23]='hintergrund1i.jpg';

var Bimg='';

function setBackground(){
  if( BIMG[ new Date().getHours() ] != Bimg ){
    //Bimg=BIMG[ new Date().getHours() ];
    $('#start').css('background-image','url(assets/img/'+ Bimg +')');
  }
  setTimeout(function(){ setBackground(); },60000 );
}

function openMessage(obj){
  $($(obj).parent().parent().parent().children()[2]).toggleClass('visible', true);
  $('body').css('overflow','hidden');
  setTimeout( function(){
      $($($(obj).parent().parent().parent().children()[2]).children()[1]).toggleClass('visible',true );
    },100 );
}


function closeMessage(obj){
  if( flag == 0 ) {
    $('body').css('overflow','initial');
    $($(obj).parent().children()[2]).toggleClass('visible', false);
    $($($(obj).parent().children()[2]).children()[1]).toggleClass('visible',false );
  }
}

$(document).on('click', '.m-menu .dropdown-menu', function(e) {
  e.stopPropagation()

})
