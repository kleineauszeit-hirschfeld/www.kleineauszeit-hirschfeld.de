---
layout: default
title: Kleine Auszeit - Hirschfeld / Wellnessmassagen und Salzgrotte !
start: true
navbar: home

meta-description: Kleine Auszeit - Hirschfeld / Wellnessmassagen und Salzgrotte !

navbar_headline: Kleine Auszeit - Hirschfeld


footer:
    blocks: 2
    logo: false
    copyright: 'Copyright &copy; 2016 | X-Corporation Designed And Developed By: <a href="https://uicookies.com/downloads/x-corporation-free-bootstrap-html-template/">uiCookies</a> and modified by Kleine Auszeit - Hirschfeld'
    icons: false

footer_blocks:
-
    title: ''
    content: '<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=13.33845376968384%2C51.03497140403255%2C13.374288082122805%2C51.04619669418911&amp;layer=mapnik&amp;marker=51.04058593866711%2C13.35637092590332" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/?mlat=51.0406&amp;mlon=13.3564#map=16/51.0406/13.3564">Größere Karte anzeigen</a></small>'


---
<style>
#start {
    background-color: antiquewhite;
}
</style>