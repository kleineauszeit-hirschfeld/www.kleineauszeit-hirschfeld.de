---
layout: default
title: Kleine Auszeit - Hirschfeld / Wellnessmassagen und Salzgrotte !
start: false
navbar: salzgrotte
meta-description: Kleine Auszeit - Hirschfeld / Wellnessmassagen und Salzgrotte !

navbar_headline: Kleine Auszeit - Hirschfeld

footer:
    blocks: 2
    logo: false
    copyright: 'Copyright &copy; 2016 | X-Corporation Designed And Developed By: <a href="https://uicookies.com/downloads/x-corporation-free-bootstrap-html-template/">uiCookies</a> and modified by Kleine Auszeit - Hirschfeld'
    icons: false

---
<style>

#start {
    background-color: rosybrown;
}

#salzgrotte {
    background: rgba(100, 66, 66, 0.6);
}
</style>